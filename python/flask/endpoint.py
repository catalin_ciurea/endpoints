#!/usr/bin/env python

__author__ = 'catalin'

from flask import Flask
import json

app = Flask(__name__)


@app.route("/")
def hello():
    return json.dumps(['first', 'second', 'third', 'fourth'])


if __name__ == "__main__":
    app.run()
