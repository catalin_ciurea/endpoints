#!/usr/bin/env perl

use strict;
use warnings;

use Mojolicious::Lite;
use JSON::XS;

get '/' => sub {
    my $self = shift;
    $self->res->headers->header('Content-Type','application/json');

    $self->render(
        text => encode_json([qw(first second third fourth)])
    );
};

app->start;

